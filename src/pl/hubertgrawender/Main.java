package pl.hubertgrawender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.DoubleStream;

public class Main {


    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\033[0;33m";
    public static final String ANSI_RESET = "\033[0m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final boolean bias = true;

    public static void main(String[] args) {
        int lay2NeuronsAmount = 3;
        int tlength = TrainingSet.get_tlength();
        //////MAKE A SWITCH FOR BIAS

        int tsetlength = TrainingSet.get_tsetlength();

///CREATION of all NEURONS
        CopyingNeuron[] cpyNeuronsList = new CopyingNeuron[tlength+1];
        for(int i = 0; i < tlength+1 ; i++) {

            if( i < tlength){
                cpyNeuronsList[i] = new CopyingNeuron();
            } else {
                cpyNeuronsList[i]= new CopyingNeuron(bias);
            }
        }


        SigmoidalNeuron[] sigNeuronsLay2 = new SigmoidalNeuron[lay2NeuronsAmount];
        for(int i = 0; i < lay2NeuronsAmount ; i++) {

            if( i < lay2NeuronsAmount-1){
                sigNeuronsLay2[i] = new  SigmoidalNeuron(tlength+1);
            } else {
                sigNeuronsLay2[i] = new  SigmoidalNeuron(tlength+1, bias);
            }
        }


        SigmoidalNeuron[] sigNeuronsLay3 = new SigmoidalNeuron[tlength];
        for(int i = 0; i < tlength ; i++) {
            sigNeuronsLay3[i] = new  SigmoidalNeuron(lay2NeuronsAmount);
        }

///START
        for(int epoch = 0; epoch < 10000; epoch++) {



            List<Integer> numbers_list = new ArrayList<>();
            for (int pattern_no = 0; pattern_no < 4; pattern_no++) { //tslength
                numbers_list.add(pattern_no);
            }
            //System.out.println(ANSI_YELLOW+"Epoch "+epoch+ANSI_RESET);
            //System.out.println("Initial collection: "+numbers_list);
            Collections.shuffle(numbers_list);
           // System.out.println("After collection: "+numbers_list);

            double[] delta = new double[sigNeuronsLay3.length];
            double[] delta2 = new double[sigNeuronsLay3.length];
            double sum[] = new double[4];

            for(int pattern_no : numbers_list){
                //System.out.print(ANSI_GREEN + "Pattern " + (pattern_no + 1) + "/" + tsetlength + ANSI_RESET);
                Perceptron.set_cpy_inputs(cpyNeuronsList, TrainingSet.ts[pattern_no]);
                Perceptron.set_sig2_inputs(sigNeuronsLay2, cpyNeuronsList);
                Perceptron.set_sig3_inputs(sigNeuronsLay3, sigNeuronsLay2);


                double[] outputs1 = Perceptron.get_cpy_outputs(cpyNeuronsList);
                double[] outputs2 = Perceptron.get_sig_outputs(sigNeuronsLay2);
                double[] outputs3 = Perceptron.get_sig_outputs(sigNeuronsLay3);


               ///////PRINTING OUTPUTS
               // System.out.println(ANSI_YELLOW+"Layer 1:"+ANSI_RESET);
                for (double output : outputs1) {
                 //   System.out.println(ANSI_BLUE+output+ANSI_RESET);
                }



                //System.out.println(ANSI_YELLOW+"Layer 2:"+ANSI_RESET);
                for (double output : outputs2) {
                //    System.out.println(ANSI_BLUE+output+ANSI_RESET);
                }

               // System.out.println(ANSI_YELLOW+"Layer 3:"+ANSI_RESET);
                for (double output : outputs3) {
                    //System.out.println(ANSI_BLUE+output+ANSI_RESET);
                }

//                for(int i = 0; i < outputs3.length; i++){
//
//                }


                for (int i = 0; i < sigNeuronsLay3.length; i++) {
                    delta[i] = TrainingSet.expectedOutput[pattern_no][i] - sigNeuronsLay3[i].getOutput();
                    delta2[i] = delta[i] * delta[i];
                }
                sum[pattern_no] = DoubleStream.of(delta2).sum();


                double[] error3 = Perceptron.error3(sigNeuronsLay3, TrainingSet.expectedOutput[pattern_no]);
                double[] error2 = Perceptron.error2(sigNeuronsLay2, sigNeuronsLay3, error3);
                Perceptron.changeWeights3(sigNeuronsLay2, sigNeuronsLay3, error3); //ZMIEN arugmentY?
                Perceptron.changeWeights2(cpyNeuronsList, sigNeuronsLay2, error2);

                //System.out.println("\n");
            }
            double ALLsum = DoubleStream.of(sum).sum();

            System.out.println(Math.round (ALLsum * 1000.0) / 1000.0);

           // for (int pattern_no = 0; pattern_no < 2; pattern_no++) { //tslength


            //}
        }

    }
}
