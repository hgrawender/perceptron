package pl.hubertgrawender;

import java.util.Random;

public class Neuron {
    private double[] weights;
    private double[] inputs;
    private double output;
    private int inputsAmount;

    Neuron(int inputsAmount) {
        this.weights = new double[inputsAmount];
        this.inputs = new double[inputsAmount];
        this.inputsAmount = inputsAmount;

        Random rand = new Random();
        for (int i = 0; i < inputsAmount; i++){
            this.inputs[i] = 0;
            this.weights[i] = rand.nextFloat()-0.5;
        }

    }

    public double[] getWeights() {
        return weights;
    }

    public void setWeights(double[] weights) {
        this.weights = weights;
    }

    public double[] getInputs() {
        return inputs;
    }

    public void setInputs(double[] inputs) {
        this.inputs = inputs;
    }

    public void setOutput(double output) {
        this.output = output;
    }

    public double getInputsAmount() {
        return inputsAmount;
    }


    public double getOutput() {
        output = 0;
        for (int i = 0; i < this.inputsAmount; i++) {
            this.output += this.inputs[i] * this.weights[i];
        }
        return this.output;
    }


}
