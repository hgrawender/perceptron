package pl.hubertgrawender;

public class SigmoidalFunction {

    public static double sigmoidal (double sum)
    {
        return 1 / (1 + Math.exp(-sum));
    }

    public static double sigmoidal_derivative (double sum) { return sigmoidal(sum) * (1 - sigmoidal(sum));
    }
}
