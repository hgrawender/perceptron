package pl.hubertgrawender;

import java.util.Random;

public class SigmoidalNeuron{

    private double[] weights;
    private double[] inputs;
    //private double error;
    private double output;
    private boolean bias = false;

    private int inputsAmount;

    SigmoidalNeuron(int inputsAmount) {
        this.weights = new double[inputsAmount];
        this.inputs = new double[inputsAmount];
        this.inputsAmount = inputsAmount;

        Random rand = new Random();
        for (int i = 0; i < inputsAmount; i++){
            this.inputs[i] = 0;
            this.weights[i] = rand.nextFloat()-0.5;
        }
    }

    SigmoidalNeuron(int inputsAmount, boolean bias) {
        this.weights = new double[inputsAmount];
        this.inputs = new double[inputsAmount];
        this.inputsAmount = inputsAmount;
        this.output = 1;
        this.bias = true;
        for (int i = 0; i < inputsAmount; i++){
            this.inputs[i] = 1;
            this.weights[i] = 1;
        }
    }

    public double[] getWeights() {
        return weights;
    }

    public void setWeights(double[] weights) {
        this.weights = weights;
    }

    public double[] getInputs() {
        return inputs;
    }

    public void setInputs(double[] inputs) {
        this.inputs = inputs;
    }

    public boolean isBias() {
        return bias;
    }

    public int getInputsAmount() {
        return inputsAmount;
    }

    public double getSig_derivative() {
        int error3 = 0;
        for (int i = 0; i < this.inputsAmount; i++) {
            this.output += this.inputs[i] * this.weights[i];
        }
        return SigmoidalFunction.sigmoidal_derivative(this.output);
    }

    public double getOutput() {
        output = 0;

        for (int i = 0; i < this.inputsAmount; i++) {
            this.output += this.inputs[i] * this.weights[i];
            //System.out.println("Inputs["+i+"]: "+this.inputs[i] + "Weights: "+ this.weights[i]);
        }
        //System.out.println("getOutput:"+this.output);
        //return SigmoidalFunction.sigmoidal(this.output);
        return (bias ? 1 : SigmoidalFunction.sigmoidal(this.output));
    }

}
