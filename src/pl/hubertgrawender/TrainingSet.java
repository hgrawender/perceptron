package pl.hubertgrawender;

import java.util.Random;

public class TrainingSet {
    public static int[][] ts = {{1, 0, 0, 0},
                                {0, 1, 0, 0},
                                {0, 0, 1, 0},
                                {0, 0, 0, 1}};

    public static int[][] expectedOutput = {{1, 0, 0, 0},
                                 {0, 1, 0, 0},
                                 {0, 0, 1, 0},
                                 {0, 0, 0, 1}};
    public static int get_tlength() {
        return TrainingSet.ts.length;
    }

    public static int get_tsetlength() {
        return TrainingSet.ts[0].length;
    }

    public static int[] RandomizeArray(int[] array){
        Random rgen = new Random();  // Random number generator

        for (int i=0; i<array.length; i++) {
            int randomPosition = rgen.nextInt(array.length);
            int temp = array[i];
            array[i] = array[randomPosition];
            array[randomPosition] = temp;
        }

        return array;
    }
}
