package pl.hubertgrawender;

import java.util.stream.DoubleStream;

public class Perceptron {
    public static final String ANSI_YELLOW = "\033[0;33m";
    public static final String ANSI_RESET = "\033[0m";

    //public static final double TRAINING_STEP = 0.15;
    public static final double TRAINING_STEP = 20;

    public static void set_cpy_inputs(CopyingNeuron[] cpyNeuronsList, int[] training_pattern) {

        for (int i = 0; i < cpyNeuronsList.length; i++) {
            if(!cpyNeuronsList[i].isBias()){            ////////////////////////////////IF
                cpyNeuronsList[i].setInput(training_pattern[i]);
            }

        }

    }

    public static double[] get_cpy_outputs(CopyingNeuron[] cpyNeuronsList) {
        double[] cpy_outputs = new double[cpyNeuronsList.length];

        for (int i = 0; i < cpyNeuronsList.length; i++) {
            cpy_outputs[i] = cpyNeuronsList[i].getOutput();
        }
        return cpy_outputs;
    }

    public static void set_sig2_inputs(SigmoidalNeuron[] sigNeuronsList, CopyingNeuron[] cpyNeuronsList) {

        for (int i = 0; i < sigNeuronsList.length; i++) {
            if(!cpyNeuronsList[i].isBias()){            ////////////////////////////////IF
                sigNeuronsList[i].setInputs(Perceptron.get_cpy_outputs(cpyNeuronsList));
            }

        }
    }

    public static void set_sig3_inputs(SigmoidalNeuron[] sigNeuronsList3, SigmoidalNeuron[] sigNeuronsList2) {
        double[] sig_outputs = Perceptron.get_sig_outputs(sigNeuronsList2);
        for (int i = 0; i < sigNeuronsList3.length; i++) {
            sigNeuronsList3[i].setInputs(sig_outputs);
        }
    }

    public static double[] get_1sig_inputs(SigmoidalNeuron sigNeuron) {

        double[] sig_inputs = sigNeuron.getInputs();
        return sig_inputs;
    }

    public static double[] get_sig_outputs(SigmoidalNeuron[] sigNeuronsList) {

        double[] sig_outputs = new double[sigNeuronsList.length];
        for (int i = 0; i < sigNeuronsList.length; i++) {
            sig_outputs[i] = 0;

            sig_outputs[i] = sigNeuronsList[i].getOutput();
        }
        return sig_outputs;
    }


/////ERRORS CALCULATION
    public static double[] error3(SigmoidalNeuron[] sigNeuronsList, int[] expectedOuput) {

        double[] sig_errors = new double[sigNeuronsList.length];
        double[] delta = new double[sigNeuronsList.length];
        double[] delta2 = new double[sigNeuronsList.length];
        for (int i = 0; i < sigNeuronsList.length; i++) {
            sig_errors[i] = sigNeuronsList[i].getSig_derivative()* (expectedOuput[i] - sigNeuronsList[i].getOutput());
            delta[i] = expectedOuput[i] - sigNeuronsList[i].getOutput();
            delta2[i] = delta[i] * delta[i];

        }
        double sum = DoubleStream.of(delta2).sum();
       // System.out.print("Sum of delta2: "+sum);
        return sig_errors;
    }

    public static double[] error2(SigmoidalNeuron[] sigNeuronsList2, SigmoidalNeuron[] sigNeuronsList3, double[] error3) {

        double[] sig_errors = new double[sigNeuronsList2.length];
        double WDeltaSum = 0;

        for (int i = 0; i < sigNeuronsList2.length; i++) {
            for (int j = 0; j < sigNeuronsList3.length; j++) {
                double[] currentNeuronWeights = sigNeuronsList3[j].getWeights();
                WDeltaSum += currentNeuronWeights[i] * error3[j];
            }

            sig_errors[i] = sigNeuronsList2[i].getSig_derivative() * WDeltaSum;
        }
        return sig_errors;
    }

    public static void changeWeights3(SigmoidalNeuron[] sigNeuronsList2, SigmoidalNeuron[] sigNeuronsList3, double[] errors3) {
        double[] outputs2 = get_sig_outputs(sigNeuronsList2);
        //System.out.println(ANSI_YELLOW+"Layer 3 weights:"+ANSI_RESET);
                                                                        //i to 3, j to 2
        for (int i = 0; i < sigNeuronsList3.length; i++) {              //for each neuron
            double[] old_weights = sigNeuronsList3[i].getWeights();
            double[] new_weights = new double[sigNeuronsList2.length];
            for (int j = 0; j < sigNeuronsList2.length; j++) {          //for each weight
               new_weights[j] =  old_weights[j] + TRAINING_STEP * errors3[i] * outputs2[j];
                //System.out.println("For lay3 neuron["+i+"], new weight["+j+"]: "+new_weights[j]);
            }
            sigNeuronsList3[i].setWeights(new_weights);
        }
    }

    public static void changeWeights2(CopyingNeuron[] cpyNeuronsList, SigmoidalNeuron[] sigNeuronsList2, double[] errors2) {
        double[] outputs1 = get_cpy_outputs(cpyNeuronsList);
        //System.out.println("sigNeuronsList2[i][j]: "+sigNeuronsList2[2].getWeights()[2]);
        //i to 3, j to 2
        //System.out.println(ANSI_YELLOW+"Layer 2 weights:"+ANSI_RESET);
        for (int i = 0; i < sigNeuronsList2.length; i++) {              //for each neuron

            double[] old_weights = sigNeuronsList2[i].getWeights();
            double[] new_weights = new double[cpyNeuronsList.length];
            for (int j = 0; j < cpyNeuronsList.length; j++) {          //for each weight

                new_weights[j] =
                        old_weights[j]
                        + TRAINING_STEP
                        * errors2[i]
                        * outputs1[j];
                //System.out.println("For lay2 neuron["+i+"], new weight["+j+"]: "+new_weights[j]);
            }
            sigNeuronsList2[i].setWeights(new_weights); //wyzeruj new_weights po tym?
        }
    }
}
