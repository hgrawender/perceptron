package pl.hubertgrawender;

import java.util.Random;

public class CopyingNeuron {

    private int weight = 1;
    private double input;
    private double output;
    private boolean bias = false;

    CopyingNeuron(){
        this.weight = 1;
    }

    CopyingNeuron(boolean bias){
        this.bias = bias ? true : false;
    }

    public double getWeight() {
        return weight;
    }


    public double getInput() {
        return input;
    }

    public boolean isBias() {
        return bias;
    }

    public void setInput(double input) {
        this.input = input;
    }


    public double getOutput() {
        output = 0;
        this.output = this.input * this.weight;
        return (bias ? 1 : this.output);
    }


}
